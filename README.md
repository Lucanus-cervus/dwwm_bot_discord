# [Python] - DWWM_Bot_Discord 

![Petite mascotte](https://adlx.education/wp-content/uploads/2019/07/0c675a8e1061478d2b7b21b330093444.gif)

Un petit bot Discord qui s'associe au Discord des étudiants Développeur Web et Web Mobile créé à l'occasion de la crise sanitaire. L'idée est de créer un bot Discord qui créé un serveur Discord adapté selon la promotion CEFIM de telle sorte à ce que chaque promo puisse profiter d'un serveur abritant ses étudiants et ses formateurs.

💡 Idées
======

Ici, sont regroupées les idées proposées par la promotion DWWM2020-1. Un code est assigné à chaque idée proposée après vote.

<!-- ✅ -->
* ✅ Validée : validée et en cours de développement
<!-- ☑️ -->
* ☑️ Candidate : bonne idée en attente de vote
<!-- ❌ -->
* ❌ Abandonnée : refusée par vote

### Modération
* Une charte d'utilisation à valider avec une réaction obligatoirement. ✅
* Système de rôles formateurs et étudiants via l'entrée d'un code selon le rôle. ✅
* Système de commandes adaptées:
    * **Formateurs**:
        * Interpeller les élèves dans un canal spécifique ✅
        * Système d'avertissements en cas de comportements ne respectant pas la charte d'utilisation du serveur Discord. ✅
    * **Élèves**:
        * Ajouter des réactions dans un canal approprié à un cours pour par exemple lever la main ou poser une question simplement avec une commande. ☑️

## Mini-Jeux
* Un Loup-Garou de Thiercelieux (un jeu que les étudiants DWWM2020-1 apprécient 😋). ☑️

⚙️ Commandes
======

Cette parti de la feuille de route est en cours de rédaction.
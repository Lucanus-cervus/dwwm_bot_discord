# REQUIRES
import discord
# Commands maker
from discord.ext import commands
from random import *

# BOT
bot = commands.Bot(command_prefix='./')

# When the bot has been connected
@bot.event
async def on_ready():
    # STATUS:
        # online
        # offline
        # idle
        # dnd or do_not_disturb
        # invisible
    await bot.change_presence(
        status=discord.Status.online,
        activity=discord.Game('./help for help')
    )
    print('[DWWM] - Je suis prêt à servir la promotion !')

# COMMAND: show the rule list
@bot.command()
async def rules(ctx):
    embed = discord.Embed(
        title='Les règles du serveur:',
        description='Une jolie charte d\'utilisation pour le serveur :partying_face: .'
    )
    await ctx.send(embed=embed)

@bot.command()
async def ping(ctx):
    await ctx.send(f'Je te reçois cinq sur cinq {ctx.message.author} :ok_hand: !')

@bot.command()
async def joke(ctx):
    nbrHasard = randint(0, 3)

    listJoke = ['Comment appelle-t-on un hamster dans l\'espace ? ||Un hamstéroïde.||', 
    'Combien il faut d’homme pour peindre une voiture en rouge ? ||Un seul, mais il faut le lancer fort.||', 
    'Pourquoi les canards sont toujours dans à l\'heure ? ||Ils sont dans l\'étang.||',
    'Pourquoi la petite fille tombe de la balançoire ? ||Parce qu\'elle n\'a pas de bras.||']

    await ctx.send(listJoke[nbrHasard])

@bot.command()
async def messageAuthor(ctx, target: discord.User):
    await target.send(f"I'll be back !")

@bot.command()
async def newChan(ctx, chanType, arg, number : int):
    guild = ctx.guild
    if chanType == "vocal":
        if len(arg) >= 3:
            embed = discord.Embed(colour=0xff0000, title="Information :", description=f'Votre channel vocal -- {arg} -- a bien été crée')
            test = {
                guild.default_role: discord.PermissionOverwrite(read_messages=False),
                guild.me: discord.PermissionOverwrite(read_messages=True)
            }

            await guild.create_vocal_channel(
                name= arg,
                overwrites= test,
                category= guild.categories[number])
            await ctx.message.author.send(embed=embed)
        else:
            embed = discord.Embed(colour=0xff0000, title="Information :", description=f'Erreur ! Votre salon vocal ne s\'est pas crée.')
            await ctx.message.author.send(embed=embed)
    elif chanType == "text":
        if len(arg) >= 3:
            embed = discord.Embed(colour=0xff0000, title="Information :", description=f'Votre channel textuel -- {arg} -- a bien été crée')
            test = {
                guild.default_role: discord.PermissionOverwrite(read_messages=False),
                guild.me: discord.PermissionOverwrite(read_messages=True)
            }

            await guild.create_text_channel(
                name= arg,
                overwrites= test,
                category= guild.categories[number])
            await ctx.message.author.send(embed=embed)
        else:
            embed = discord.Embed(colour=0xff0000, title="Information :", description=f'Erreur ! Votre salon textuel ne s\'est pas crée.')
            await ctx.message.author.send(embed=embed)
    else:
        return False

# COMMANDE: nouvelle catégorie de channel.
@bot.command()
async def newCat(ctx, arg: str):
    guild = ctx.guild
    embedTrue = discord.Embed(colour=0xff0000, title="Information :", description=f'Votre catégorie -- {arg} -- a bien été crée.')
    embedFalse = discord.Embed(colour=0xff0000, title="Information :", description=f'Erreur ! Votre nom de catégorie doit contenir plus de 2 caractères.')
    if len(arg) >= 3:
        await guild.create_category_channel(arg, overwrites=None)
        await ctx.message.author.send(embed=embedTrue)
    else:
        await ctx.message.author.send(embed=embedFalse)

# COMMANDE: liste des catégories de channel.
@bot.command()
async def idChan(ctx):
    embed = discord.Embed(colour=0xff0000, title="Voici les noms de toutes les catégories présentes sur le Discord :")
    for i in range(len(ctx.guild.categories)):
        embed_value = f"""
            {ctx.guild.categories[i].name}
        """

        embed.add_field(
            name=f'{i} :', value=embed_value, inline=False
        )

    await ctx.send(embed=embed)

# COMMAND: poke somebody on the server
@bot.command()
async def poke(ctx, target: discord.Member):
    await target.send(f"{ctx.author.mention} t'as envoyé un poke, il a peut-être besoins de ton aide !")

# If no Member in arg
@poke.error
async def on_command_error(ctx, error):
    if isinstance(error, commands.MissingRequiredArgument):
        embed = discord.Embed(colour=0xff0000)
        embed_value = """
        ```./poke @membreCible``` - Permet de poker un membre du erveur grâce à son nom.
        """
        embed.add_field(
            name='**Rappel**', value=embed_value, inline=False
        )
        await ctx.send(embed=embed)

# COMMAND: clear the current channel
@bot.command()
async def clear(ctx, limit=None):
    if limit:
        await ctx.message.channel.purge(
            limit=limit
        )
        await ctx.send(f'Je viens de supprimer les {limit} derniers messages du canal !')
    else:
        await ctx.message.channel.purge()
        await ctx.send(f'Je viens de supprimer tous les messages du canal !')

# Token de connexion au serveur
TOKEN = ''
bot.run(TOKEN)